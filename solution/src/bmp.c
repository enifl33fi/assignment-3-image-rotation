#include "bmp.h"
#include <malloc.h>

#define BF_TYPE 0x4D42
#define BF_DATA_OFFSET 54
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BI_COMPRESSION 0
#define BI_PELS_PER_METER 0
#define BI_CRL_USED 0
#define BI_CRL_IMPORTANT 0

struct __attribute__((packed)) 
bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum read_status from_bmp( FILE* in, struct image* img ) {
  struct bmp_header header = {0};

  if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
    if (feof(in) != 0) {
      return READ_UNEXPECTED_EOF;
    }
    return READ_INVALID_HEADER;
  }

  if (header.bfType != BF_TYPE) {
    return READ_INVALID_SIGNATURE;
  }

  size_t img_size = get_extended_size(header.biWidth * sizeof(struct pixel), header.biHeight);
  uint8_t* colors = (uint8_t*) malloc(img_size);
  if (fread(colors, img_size, 1, in) != 1) {
    free(colors);
    if (feof(in) != 0) {
      return READ_UNEXPECTED_EOF;
    }
    return READ_INVALID_DATA;
  }
  
  *img = create_image(colors, header.biWidth, header.biHeight);
  free(colors);

  return READ_OK;
}

static struct bmp_header create_header(struct image const* img) {
  return (struct bmp_header){
    .bfType = BF_TYPE,
    .bfileSize = BF_DATA_OFFSET + get_extended_size(img->width * sizeof(struct pixel), img->height),
    .bfReserved = 0,
    .bOffBits = BF_DATA_OFFSET,
    .biSize = BI_SIZE,
    .biWidth = img->width,
    .biHeight = img->height,
    .biPlanes = BI_PLANES,
    .biBitCount = BI_BIT_COUNT,
    .biCompression = BI_COMPRESSION,
    .biSizeImage = get_extended_size(img->width * sizeof(struct pixel), img->height),
    .biXPelsPerMeter = BI_PELS_PER_METER,
    .biYPelsPerMeter = BI_PELS_PER_METER,
    .biClrUsed = BI_CRL_USED,
    .biClrImportant = BI_CRL_IMPORTANT
  };
}

enum write_status to_bmp( FILE* out, struct image const* img) {
  int32_t zero = 0;
  uint32_t padding = get_padding(img->width * sizeof(struct pixel));
  struct bmp_header header = create_header(img);

  if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
    return WRITE_HEADER_ERROR;
  }

  for (uint32_t i = 0; i < img->height; i++) {
    for (uint32_t j = 0; j < img->width; j++) {
      if (fwrite(&img->data[get_pixel_index(img->width, i, j)], sizeof(struct pixel), 1, out) != 1) {
        return WRITE_ERROR;
      }
    }
    if (padding != 0) {
      if (fwrite(&zero, padding, 1, out) != 1) {
        return WRITE_ERROR;
      }
    }
  }
  return WRITE_OK;
}
