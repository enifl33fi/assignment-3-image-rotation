#include "format_io.h"
#include "image.h"
#include "parse.h"
#include "rotate.h"
#include <malloc.h>
#include <stdio.h>

int main( int argc, char** argv ) {   
    int16_t ang = 0;

    int status = parse_args(argc, argv, &ang);
    if (status != 0) {
        fprintf(stderr, "Parsing finished with code %d\n", status);
        return status;
    }

    char* source = argv[1];
    char* output = argv[2];

    struct image img = {0};

    status = read_bmp(source, &img);
    if (status != 0) {
        fprintf(stderr, "Reading finished with code %d\n", status);
        return status;
    }

    struct image rotated_img = rotate_by_angle(img, ang);
    free(img.data);

    status = write_bmp(output, &rotated_img);
    if (status != 0) {
        fprintf(stderr, "Writing finished with code %d\n", status);
        free(rotated_img.data);
        return status;
    }
    free(rotated_img.data);

    return 0;
}
