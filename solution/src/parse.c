#include "parse.h"
#include "util.h"
#include <stdlib.h>

enum parse_status parse_args(int const argc, char** const argv, int16_t* ang) {
    if (argc != 4) {
        return PARSE_INVALID_ARG_COUNT;
    }

    if (!is_num(argv[3])) {
        return PARSE_INVALID_ANGLE;
    }
    
    char* end;
    int16_t angle = (int16_t) strtol(argv[3], &end, 10);

    if (angle > 270 || angle < -270) {
        return PARSE_INVALID_ANGLE;
    }
    if (angle % 90 != 0) {
        return PARSE_INVALID_ANGLE;
    }

    *ang = angle;

    return PARSE_OK;
}
