#include "bmp.h"
#include "format_io.h"

enum read_status read_bmp(const char* bmp_filename, struct image* img) {
    if (!bmp_filename) {
        return READ_INVALID_FILENAME;
    }

    FILE* bmp_file = fopen(bmp_filename, "rb");
    if (!bmp_file) {
        return READ_INVALID_FILENAME;
    }

    enum read_status res_code = from_bmp(bmp_file, img);
    fclose(bmp_file);
    return res_code;
}

enum write_status write_bmp(const char* bmp_filename, struct image const* img) {
    if (!bmp_filename) {
        return WRITE_INVALID_FILENAME;
    }

    FILE* bmp_file = fopen(bmp_filename, "wb");
    if (!bmp_file) {
        return WRITE_CREATE_ERROR;
    }

    enum write_status res_code = to_bmp(bmp_file, img);
    fclose(bmp_file);
    return res_code;
}
