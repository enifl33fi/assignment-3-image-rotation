#include "image.h"
#include <malloc.h>
#include  <stdint.h>


uint32_t get_padding(const uint32_t width) {
  return width % 4 == 0 ? 0 : 4 - width % 4;
}

static uint32_t get_extended_width(const uint32_t width) {
  return width + get_padding(width);
}

uint64_t get_extended_size(const uint32_t width, const uint32_t height) {
  return get_extended_width(width) * height;
}

static uint64_t get_size(const uint32_t width, const uint32_t height) {
  return width * height;
}

uint64_t get_pixel_index(const uint32_t width, const uint32_t row, const uint32_t column) {
  return row * width + column;
}

static struct pixel create_pixel(uint8_t const* const color) {
  return (struct pixel) {color[0], color[1], color[2]};
}
struct image create_empty_image(const uint32_t width, const uint32_t height) {
  struct pixel* pixels = (struct pixel*) malloc(get_size(width, height) * sizeof(struct pixel));
  struct image img = {width, height, pixels};
  return img;
}

struct image create_image(uint8_t const* colors, const uint32_t width, const uint32_t height) {
  struct image img = create_empty_image(width, height);
  uint32_t padding = get_padding(width * sizeof(struct pixel));
  for (uint32_t i = 0; i < height; i++) {
    for (uint32_t j = 0; j < width; j++) {
      img.data[get_pixel_index(width, i, j)] = create_pixel(colors);
      colors += sizeof(struct pixel);
    }
    colors += padding;
  }
  return img;
}
