#include "util.h"

bool is_num(char* const str) {
    if (!str) {
        return false;
    }

    char* cur = str;

    if (*cur == '-') {
        cur++;
    }

    if (*cur == 0) {
        return false;
    }

    while (*cur != 0) {
        if (*cur < '0' || *cur > '9') {
            return false;
        }
        cur++;
    }

    return true;

}
