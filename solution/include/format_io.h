#include "image.h"
#include "io_status.h"


#ifndef _FORMAT_IO_H_
#define _FORMAT_IO_H_

enum read_status read_bmp(const char* bmp_filename, struct image* img);

enum write_status write_bmp(const char* bmp_filename, struct image const* img);

#endif
