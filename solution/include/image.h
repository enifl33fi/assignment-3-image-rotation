#include  <stdint.h>


#ifndef _IMAGE_H_
#define _IMAGE_H_

struct pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};

uint32_t get_padding(const uint32_t width);

uint64_t get_extended_size(const uint32_t width, const uint32_t height);

uint64_t get_pixel_index(const uint32_t width, const uint32_t row, const uint32_t column); 

struct image create_empty_image(const uint32_t width, const uint32_t height);

struct image create_image(uint8_t const* colors, const uint32_t width, const uint32_t height);

#endif
