#include "image.h"
#include "io_status.h"
#include <stdio.h>


#ifndef _BMP_H_
#define _BMP_H_

enum read_status from_bmp( FILE* in, struct image* img );

enum write_status to_bmp( FILE* out, struct image const* img );

#endif
