#ifndef _PARSE_STATUS_H_
#define _PARSE_STATUS_H_

enum parse_status  {
  PARSE_OK = 0,
  PARSE_INVALID_ARG_COUNT,
  PARSE_INVALID_ANGLE,
  PARSE_ERROR
  };

#endif
