#include "parse_status.h"
#include  <stdint.h>


#ifndef _PARSE__H_
#define _PARSE__H_

enum parse_status parse_args(int const argc, char** const argv, int16_t* ang);

#endif
