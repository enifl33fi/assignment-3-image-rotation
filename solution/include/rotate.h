#include "image.h"

#ifndef _ROTATE_H_
#define _ROTATE_H_

struct image rotate_by_angle(struct image const source, int16_t const angle);

#endif
